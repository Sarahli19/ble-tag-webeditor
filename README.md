# About

This is web editor to write to the Bluetooth LED name badge. The ideas are mainly based on the insights of this article https://nilhcem.com/iot/reverse-engineering-bluetooth-led-name-badge

The app uses the Bluetooth Web API which is as of now still an experimental feature. For further info check:

- [Browser compatibility](https://developer.mozilla.org/en-US/docs/Web/API/Web_Bluetooth_API#browser_compatibility)
- [Chrome docs](https://developer.chrome.com/docs/capabilities/bluetooth?hl=en)

# Getting started

## Enable experimental bluetooth

### Chrome:

- Go to [chrome://flags/#enable-experimental-web-platform-features](chrome://flags/#enable-experimental-web-platform-features)
- Enable experimental web platform features
- Restart Chrome

### Trouble shooting

If you start the application and click on "Send text" a new dialog should pop up, showing you, that the browser is search for nearby bluetooth devices. There is currently no exception handling or hint in case the bluetooth functionality is disabled - it will just do nothing :)

## Start app

Node v20.11.0

- npm install
- npm run dev
