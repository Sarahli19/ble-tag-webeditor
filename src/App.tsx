import "./App.scss";
import "./styles/global.scss";

import { TextToHexString } from "./TextToHexString/TextToHexString";
import { DeviceWriter } from "./DeviceWriter/DeviceWriter";
import { FieldValues, useForm } from "react-hook-form";
import { MODE } from "./TextToHexString/Codes";
import Bitmap from "./Bitmap/Bitmap";

function App() {
  const { register, handleSubmit } = useForm({
    defaultValues: {
      speed: 1,
      mode: "0",
      text: "",
      flash: false,
      marquee: false,
    },
  });

  const send = async (data: FieldValues) => {
    const w = await DeviceWriter.init();
    w.send(
      TextToHexString.build(
        data.text,
        data.flash,
        data.marquee,
        data.speed,
        data.mode
      )
    );
  };

  const sendBytes = async (bytes: number[]) => {
    const w = await DeviceWriter.init();
    console.log(TextToHexString.buildBitMap(bytes));
    w.send(TextToHexString.buildBitMap(bytes));
  };

  return (
    <div className="main">
      <h2 className="title">BLE TAG Writer</h2>
      <div className="card">
        <form onSubmit={handleSubmit((data) => send(data))}>
          <input {...register("text")} type="text" placeholder="Text to send" />

          <label className="flash">
            Flash it! <input {...register("flash")} type="checkbox" />
          </label>
          <label className="marquee">
            Marquee it!
            <input {...register("marquee")} type="checkbox" />
          </label>
          <input
            {...register("speed", { min: 1, max: 8, required: true })}
            type="number"
            placeholder="Speed"
            className="speed"
          />
          <select {...register("mode", { required: true })}>
            <option value="">Mode</option>
            {Object.entries(MODE).map(([key, value]) => (
              <option key={key} value={value}>
                {key}
              </option>
            ))}
          </select>
          <button>Send Text 🚀</button>
        </form>
        <hr />
        <div>
          <Bitmap request={(bytes) => sendBytes(bytes)} />
        </div>
      </div>
    </div>
  );
}

export default App;
