import { hexStringToByteArray } from "../utils";

const SERVICE_UUID = "0000fee0-0000-1000-8000-00805f9b34fb";
const CHARACTERISTIC_UUID = "0000fee1-0000-1000-8000-00805f9b34fb";
const CHUNK_SIZE = 16;

export class DeviceWriter {
  #device: BluetoothDevice | undefined;
  #characteristic: BluetoothRemoteGATTCharacteristic | undefined;

  static async init() {
    const writer = new DeviceWriter();
    await writer.init();
    return writer;
  }

  private async init() {
    this.#device = await this.requestDevice();
  }

  private async requestDevice(): Promise<BluetoothDevice> {
    return navigator.bluetooth?.requestDevice({
      filters: [{ namePrefix: "LS" }],
      acceptAllDevices: false,
      optionalServices: [SERVICE_UUID],
    });
  }

  private async connect() {
    this.#characteristic = await this.#device?.gatt
      ?.connect()
      .then((server?: BluetoothRemoteGATTServer) =>
        server?.getPrimaryService(SERVICE_UUID)
      )
      .then((service?: BluetoothRemoteGATTService) =>
        service?.getCharacteristic(CHARACTERISTIC_UUID)
      );
  }

  async disconnect() {
    this.#device?.gatt?.disconnect();
  }

  async send(message: string) {
    await this.connect();
    const buffer = this.convertMessage(message);
    for await (const chunk of buffer) {
      await this.#characteristic?.writeValueWithResponse(chunk);
    }
  }

  async sendBytes(byteData: number[]) {
    await this.connect();
    const chunks: number[][] = [];

    for (let i = 0; i < byteData.length; i += CHUNK_SIZE) {
      chunks.push(byteData.slice(i, i + CHUNK_SIZE));
    }

    const buffer = chunks.map((chunk) => {
      const uint8Array = new Uint8Array(chunk);
      return uint8Array.buffer;
    });

    for await (const chunk of buffer) {
      await this.#characteristic?.writeValueWithResponse(chunk);
    }
  }

  private convertMessage(message: string): ArrayBufferLike[] {
    const byteData = hexStringToByteArray(message);
    const chunks: number[][] = [];

    for (let i = 0; i < byteData.length; i += CHUNK_SIZE) {
      chunks.push(byteData.slice(i, i + CHUNK_SIZE));
    }

    return chunks.map((chunk) => {
      const uint8Array = new Uint8Array(chunk);
      return uint8Array.buffer;
    });
  }
}
