export function hexStringToByteArray(hex: string) {
  const byteArray = [];
  for (let i = 0; i < hex.length; i += 2) {
    byteArray.push(parseInt(hex.slice(i, i + 2), 16));
  }
  return byteArray;
}

export const toHexString = (bytes: number[]) => {
  return Array.from(bytes, (byte) => {
    return ("0" + (byte & 0xff).toString(16)).slice(-2);
  }).join("");
};

export const range = (end: number): number[] => [...Array(end).keys()];
