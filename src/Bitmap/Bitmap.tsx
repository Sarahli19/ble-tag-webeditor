import { useState } from "react";
import styles from "./Bitmap.module.scss";
import { Canvas, Row } from "./Canvas";
import Pixel from "./Pixel";

type BitmapProps = {
  request: (bytes: number[]) => void;
};

const Bitmap = ({ request }: BitmapProps) => {
  const [canvas, setCanvas] = useState<Canvas>(Canvas.build());
  return (
    <>
      <div>
        {canvas.getRows().map((row: Row, i: number) => (
          <div key={i} className={styles.row}>
            {row.map((_, j: number) => (
              <Pixel
                key={`${i}_${j}`}
                filled={canvas.isFilled(i, j)}
                onToggle={(filled: boolean) => canvas.toggle(i, j, filled)}
              />
            ))}
          </div>
        ))}
      </div>
      {/* TODO add functionality */}
      <button onClick={() => setCanvas(Canvas.build())}>Reset</button>
      <button
        style={{ marginTop: "1rem" }}
        onClick={() => {
          request(canvas.getAsBytes());
        }}
      >
        Send Bitmap 🌈
      </button>
    </>
  );
};

export default Bitmap;
