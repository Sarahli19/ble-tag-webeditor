import { range } from "../utils";

export type Row = boolean[];
export type Pixels = Row[];

const ROWS = 11;
const COLS = 40;
const BITS_PER_BYTE = 8;

export class Canvas {
  #pixels: Pixels = [];

  static build(): Canvas {
    return new Canvas();
  }

  private constructor() {
    this.init();
  }

  private init(): void {
    this.#pixels = [];
    const initRow = range(COLS).map(() => false);
    range(ROWS).forEach(() => this.#pixels.push([...initRow]));
  }

  restore(data: Pixels): void {
    this.#pixels = data;
  }

  reset(): void {
    this.init();
  }

  getRows(): Row[] {
    return this.#pixels;
  }

  isFilled(i: number, j: number): boolean {
    return this.#pixels[i][j];
  }

  toggle(i: number, j: number, filled: boolean): void {
    this.#pixels[i][j] = filled;
  }

  getAsBytes(): number[] {
    const bytes: number[] = [];
    for (let i = 0; i < COLS / BITS_PER_BYTE; i++) {
      for (let row = 0; row < ROWS; row++) {
        let byte = 0;
        for (let col = 0; col < BITS_PER_BYTE; col++) {
          const isOn = this.isFilled(row, i * BITS_PER_BYTE + col);
          byte = byte | ((isOn ? 1 : 0) << (BITS_PER_BYTE - 1 - col));
        }
        bytes.push(byte);
      }
    }
    return bytes;
  }
}
