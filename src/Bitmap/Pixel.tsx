import { useEffect, useState } from "react";
import styles from "./Pixel.module.scss";
import cx from "classnames";

type PixelProps = {
  filled: boolean;
  onToggle: (filled: boolean) => void;
};

type EventStatus = {
  mousedown: boolean;
  hovered: boolean;
};

const Pixel = ({ filled: filledInit, onToggle }: PixelProps) => {
  const [filled, setFilled] = useState(filledInit);
  const [event, setEvent] = useState<EventStatus>({
    mousedown: false,
    hovered: false,
  });

  useEffect(() => {
    onToggle(filled);
  }, [filled, onToggle]);

  useEffect(() => {
    setFilled(filledInit);
  }, [filledInit, onToggle]);

  useEffect(() => {
    const onmousedown = () => setEvent((x) => ({ ...x, mousedown: true }));
    document.body.addEventListener("mousedown", onmousedown);
    const onmouseup = () => setEvent((x) => ({ ...x, mousedown: false }));
    document.body.addEventListener("mouseup", onmouseup);
    return () => {
      document.body.removeEventListener("mousedown", onmousedown);
      document.body.removeEventListener("mouseup", onmouseup);
    };
  }, []);

  useEffect(() => {
    if (event.hovered && event.mousedown) {
      setFilled(true);
    }
  }, [event]);

  const onMouseEnter = (entered: boolean) => {
    setEvent((x) => ({ ...x, hovered: entered }));
  };

  return (
    <div
      className={cx(styles.pixel, filled ? styles["pixel--filled"] : "")}
      onMouseEnter={() => onMouseEnter(true)}
      onMouseLeave={() => onMouseEnter(false)}
    />
  );
};

export default Pixel;
