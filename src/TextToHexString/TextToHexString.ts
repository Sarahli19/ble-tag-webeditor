import { toHexString } from "../utils";
import { CHAR_CODES, Mode } from "./Codes";

const BREAK_LINE = "00000000000000000000000000000000";
const HEADER_LINE = "77616E670000";

const TIMESTAMP_LINE = "000000000000E10C0617363300000000";

const numberOfFillerZeros = (textLength: number): number => {
  return 32 - (textLength % 32);
};

const getTextLengthAsHexString = (text: string): string => {
  const length = text.length;
  const hexString = length.toString(16);
  return hexString.padStart(4, "0");
};

const mapTextToCharacterCode = (text: string): string => {
  let hexString = "";
  Array.from(text).forEach((char) => {
    hexString += mapCharacterToCharacterCode(char);
  });
  hexString += "0".repeat(numberOfFillerZeros(hexString.length));
  return hexString;
};

const mapCharacterToCharacterCode = (character: string): string => {
  return CHAR_CODES[character as keyof typeof CHAR_CODES];
};

export class TextToHexString {
  private hexString: string;

  private constructor() {
    this.hexString = HEADER_LINE;
  }

  private append(str: string): TextToHexString {
    this.hexString += str;
    return this;
  }

  private build(): string {
    return this.hexString + BREAK_LINE;
  }

  private fillWithZeros(): TextToHexString {
    this.hexString += "0".repeat(numberOfFillerZeros(this.hexString.length));
    return this;
  }

  public static buildBitMap(bitmap: number[]): string {
    return (
      new TextToHexString()
        // 0000040000000000000 000050000000000000000000000000000000000000000
        // the 4 is for the mode - great for debugging but 4 is fixed and perfect for the bitmap
        .append("0000040000000000000" + "000050000000000000000000000000000")
        .append(TIMESTAMP_LINE)
        .append(BREAK_LINE)
        .append(toHexString(bitmap))
        .fillWithZeros()
        .build()
    );
  }

  public static build(
    text: string,
    flash = false,
    marquee = false,
    speed = 1,
    mode = Mode.LEFT
  ): string {
    return new TextToHexString()
      .append(flash ? "01" : "00")
      .append(marquee ? "01" : "00")
      .append((speed - 1).toString())
      .append(mode)
      .append("0".repeat(14))
      .append(getTextLengthAsHexString(text).padEnd(32, "0"))
      .append(TIMESTAMP_LINE)
      .append(BREAK_LINE)
      .append(mapTextToCharacterCode(text))
      .build();
  }
}
